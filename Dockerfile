FROM ubuntu:latest

LABEL maintainer="Pierre Labadens <labadens.pierre@gmail.com>"

ARG PANDOC_VERSION=2.3

# install latex packages
RUN apt-get update -y \
  && apt-get install -y --no-install-recommends \
    texlive-latex-recommended \
    texlive-luatex \
    texlive-fonts-recommended \
    texlive-fonts-extra \
    fontconfig \
    lmodern \
    make \
    curl \
    ca-certificates

RUN VER=${PANDOC_VERSION}; \
    curl -SL "https://github.com/jgm/pandoc/releases/download/${VER}/\
pandoc-${VER}-1-amd64.deb" > pandoc.deb \
  && dpkg -i pandoc.deb \
  && rm -rf pandoc.deb \
  && apt-get purge -y curl ca-certificates \
  && apt-get autoremove -y \
  && apt-get clean -y

VOLUME /builds

WORKDIR /builds

CMD ["pandoc"]
