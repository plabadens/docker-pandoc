# docker-pandoc

[![pipeline status](https://gitlab.com/plabadens/docker-pandoc/badges/master/pipeline.svg)](https://gitlab.com/plabadens/docker-pandoc/commits/master)

Docker image for pandoc building.
